app.controller('PaperTypeController', function($scope, $rootScope, Sumprice, $timeout, $translate) {
  let vm = this;
  let TEXT_TRANSLATED = $translate.instant('PAPER_TYPE_GLOSS');
  let TEXT_TRANSLATED_MT = $translate.instant('PAPER_TYPE_MAT');

    vm.items = [{
        id: 0,
        name: TEXT_TRANSLATED
      },{
        id: 1,
        name: TEXT_TRANSLATED_MT
      }];

 vm.items[1];
     
 let tmpp = vm.items[0];

   vm.item;
 
     $scope.$on('myCustomEvent', function (event, data) {
        for (let i = vm.items.length - 1; i >= 0; i--) {
         let trt = vm.items[i];

         if (trt.name == data.paper) {
          vm.item = vm.items[i];
          vm.callback(vm.item)
          } 
        }
      });

  var countUp = function() {
    Sumprice.totalPaper(tmpp.name, vm.innerIndex);
    Sumprice.totalPrice();
  }
  $timeout(countUp, 100);

     vm.callback = function(item, index) {
      vm.fromCallback = item.name;
      $scope.$watch(function() {
            return item;
          }, function watcrootScopehCallback(newValue, oldValue) {
              Sumprice.totalPaper(vm.item.name, index);
              Sumprice.totalPrice();
          });
    };
});