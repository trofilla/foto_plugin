(function () {
    'use strict';

    angular
        .module('app')
        .controller('FormatController', FormatController);

   FormatController.$inject = ['$scope', '$rootScope', 'Sumprice', '$timeout'];
    function FormatController($scope, $rootScope, Sumprice, $timeout) {
       let index = 0;



    var vm = this;
    vm.items = [{
        id: 0,
        name: '10 x 15',
        price: 1.90, 
        hi_price: 2.20
      },{
        id: 1,
        name: '9 x 13',
        price: 1.90,
        hi_price: 2.20
      },{
        id: 2,
        name: '13 x 18',
        price: 3.60,
        hi_price: 2.20
      },{
        id: 3,
        name: '15 x 21',
        price: 3.80,
        hi_price: 2.20
      },{
        id: 4,
        name: '18 x 25',
        price: 7.20,
        hi_price: 2.20
      },{
        id: 5,
        name: '21 x 30',
        price: 7.60,
        hi_price: 2.20
      },{
        id: 6,
        name: '30 x 40',
        price: 18.00,
        hi_price: 2.20
      },{
        id: 7,
        name: '30 x 64',
        price: 30.00,
        hi_price: 2.20
      }];

     // current item
    // vm.item = null; 
    vm.items[1];
    let qqq = 1;

$scope.$on('CounterEvent', function(event, data) {
      let tmpp = vm.items[0];
      if (data<=3) {
            var countUp = function() {
                Sumprice.formatPrice(tmpp.price, vm.innerIndex);
                Sumprice.totalFormat(tmpp.name, vm.innerIndex);
                Sumprice.totalPrice();
              }
              $timeout(countUp, 100);

            $scope.$on('myCustomEvent', function (event, data) {
              for (var i = vm.items.length - 1; i >= 0; i--) {
               let trt = vm.items[i];

               if (trt.name == data.format) {
                vm.item = vm.items[i];
                vm.callback(vm.item);
                Sumprice.formatPrice(vm.item.price, vm.innerIndex);
                    Sumprice.totalFormat(vm.item.name, vm.innerIndex);
                    Sumprice.totalPrice();
                } 
              };
            });

            vm.callback = function(item, index) {
              vm.fromCallback = item.name;
              $scope.$watch(function() {
                    return item;
                  }, function watcrootScopehCallback(newValue, oldValue) {
                      Sumprice.formatPrice(vm.item.price, index);
                      Sumprice.totalFormat(vm.item.name, index);
                      Sumprice.totalPrice();
                  });
            };
          }

      else {
          var countUp = function() {
                Sumprice.formatPrice(tmpp.hi_price, vm.innerIndex);
                Sumprice.totalFormat(tmpp.name, vm.innerIndex);
                Sumprice.totalPrice();
              }
              $timeout(countUp, 100);

            $scope.$on('myCustomEvent', function (event, data) {
              for (var i = vm.items.length - 1; i >= 0; i--) {
               let trt = vm.items[i];

               if (trt.name == data.format) {
                vm.item = vm.items[i];
                vm.callback(vm.item);
                Sumprice.formatPrice(vm.item.hi_price, vm.innerIndex);
                    Sumprice.totalFormat(vm.item.name, vm.innerIndex);
                    Sumprice.totalPrice();
                } 
              };
            });

            vm.callback = function(item, index) {
              vm.fromCallback = item.name;
              $scope.$watch(function() {
                    return item;
                  }, function watcrootScopehCallback(newValue, oldValue) {
                      Sumprice.formatPrice(vm.item.hi_price, index);
                      Sumprice.totalFormat(vm.item.name, index);
                      Sumprice.totalPrice();
                  });
            };
      }
    });
  }

})();