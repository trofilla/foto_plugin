var app = angular.module('app');
 

var translationsRU = {
 FOTOXATA_SYHIV: '"Фотохата"- Сыховская, 11',
  FOTOXATA_PETLURU: '"Фотохата"- Петлюри, 2',
  FOTOXATA_FRANKA: '"Фотохата"- Франка, 71',
  FOTOXATA_GRINCHENKA: '"Фотохата"- Гринченко 11Б',
  FOTOXATA_NAUKOVA: '"Фотохата"- Научная, 59',
  FOTOXATA_SYBOTIVSKA: '"Фотохата"- Суботовская, 7',
  FOTOXATA_TUCHUNU: '"Фотохата"- Тычины, 14',
  FOTOXATA_WITH_DELIVERY_SYHIV: 'По Сихову - 10 грн',
  FOTOXATA_WITH_DELIVERY_LWIW: 'По Львову - 15 грн',
  FOTOXATA_WITH_DELIVERY_POST: 'Почтой + стоимость доставки',
  PHONE_NUMBER: 'Телефон',
  CUSTOMER_NAME: 'Имя',
  YOUR_PHONE_NUMBER: 'Введите актуальный номер телефона',
  HEADLINE: 'Заполните форму',
  PRUMITKA: 'Дополнительная информация',
  ORDER: 'Оформление заказа',
  LOAD_FILE: 'Загрузить файл',
  LOAD_FOLDER: 'Загрузить папку',
  YES: 'Да',
  NO: 'Нет',
  BUTTON_LANG_RU: 'Русский',
  BUTTON_LANG_UK: 'Украинский', 
  BUTTON_LANG_EN: 'Английский',
  PAPER_TYPE_GLOSS: 'Глянцевая',
  PAPER_TYPE_MAT: 'Матовая',
  DOSTAVKA: 'Доставка',
  DOSTAVKA_DATE: 'Заказ будет виполнен',
  DROP_PLACE: 'Перетените файлы сюда',
  TOTAL_PRICE: 'Общая сумма',
  FOR_ALL: 'Для всех',
  CANCEL: 'Отмена',
  YOUR_PHOTO: 'Ваше фото',
  FILE_NAME: 'Имя файла',
  PHOTO_QTY: 'Количество',
  PHOTO_FORMAT: 'Формат',
  PAPER_TYP: 'Бумага',
  PHOTO_RAMKA: 'Рамка',
  COMPLITE_ORDER: 'Оформить заказ', 
  SET_ALL_FORDEFAULT: 'Использовать данные параметры для всех фотографий'
};
 
var translationsUK= {
  FOTOXATA_SYHIV: '"Фотохата"- Сихівська,11',
  FOTOXATA_PETLURU: '"Фотохата"- Петлюри, 2',
  FOTOXATA_FRANKA: '"Фотохата"- Франка, 71',
  FOTOXATA_GRINCHENKA: '"Фотохата"- Грінченка 11Б',
  FOTOXATA_NAUKOVA: '"Фотохата"- Наукова, 59',
  FOTOXATA_SYBOTIVSKA: '"Фотохата"- Суботівська, 7',
  FOTOXATA_TUCHUNU: '"Фотохата"- Тичини, 14',
  FOTOXATA_WITH_DELIVERY_SYHIV: 'По Сихову - 10 грн',
  FOTOXATA_WITH_DELIVERY_LWIW: 'По Львову - 15 грн',
  FOTOXATA_WITH_DELIVERY_POST: 'Поштою + вартість доставки',
  PHONE_NUMBER: 'Телефон',
  CUSTOMER_NAME: 'Им\'я',
  YOUR_PHONE_NUMBER: 'Введіть дійсний номер телефона',
  HEADLINE: 'Заповніть форму',
  PRUMITKA: 'Додаткова інформація',
  ORDER: 'Оформлення замовлення',
  LOAD_FILE: 'Завантажити файл',
  LOAD_FOLDER: 'Завантажити папку',
  YES: 'Так',
  NO: 'Ні',
  BUTTON_LANG_RU: 'Російська',
  BUTTON_LANG_EN: 'Англійська',
  BUTTON_LANG_UK: 'Українська',
  PAPER_TYPE_GLOSS: 'Глянцевий',
  PAPER_TYPE_MAT: 'Матовий',
  DOSTAVKA: 'Доставка',
  DOSTAVKA_DATE: 'Замовлення буде виконане',
  DROP_PLACE: 'Перетягніть свою фото сюди',
  TOTAL_PRICE: 'Загальна сума',
  FOR_ALL: 'Для всіх',
  CANCEL: 'Відмінити',
  YOUR_PHOTO: 'Ваше фото',
  FILE_NAME: 'Назва файла',
  PHOTO_QTY: 'Кількість',
  PHOTO_FORMAT: 'Формат',
  PAPER_TYP: 'Папір',
  PHOTO_RAMKA: 'Рамка',
  COMPLITE_ORDER: 'Оформити замовлення',
  SET_ALL_FORDEFAULT: 'Застосувати налаштування для всіх фотографій'
};
 
var translationsEN= {
  FOTOXATA_SYHIV: '"Fotoxata"- Syhivska,11',
  FOTOXATA_PETLURU: '"Fotoxata"- Petlyru, 2',
  FOTOXATA_FRANKA: '"Fotoxata"- Franka, 71',
  FOTOXATA_GRINCHENKA: '"Fotoxata"- Grinchenka 11Б',
  FOTOXATA_NAUKOVA: '"Fotoxata"- Naukova, 59',
  FOTOXATA_SYBOTIVSKA: '"Fotoxata"- Sybotivska, 7',
  FOTOXATA_TUCHUNU: '"Fotoxata"- Tuchunu, 14',
  FOTOXATA_WITH_DELIVERY_SYHIV: 'Syhiv delivery - 10 uah',
  FOTOXATA_WITH_DELIVERY_LWIW: 'Lviv delivery - 15 uah',
  FOTOXATA_WITH_DELIVERY_POST: 'UkrPost delivery + delivery price',
  PHONE_NUMBER: 'Phone number',
  CUSTOMER_NAME: 'Name',
  YOUR_PHONE_NUMBER: 'Youe phone number',
  HEADLINE: 'Please fill form',
  PRUMITKA: 'Additionl information',
  ORDER: 'Order',
  LOAD_FILE: 'Upload file',
  LOAD_FOLDER: 'Upload folder',
  YES: 'Yes',
  NO: 'No',
  BUTTON_LANG_RU: 'Russian',
  BUTTON_LANG_EN: 'English',
  BUTTON_LANG_UK: 'Ukrainian',
  PAPER_TYPE_GLOSS: 'Glossy',
  PAPER_TYPE_MAT: 'Matte',
  DOSTAVKA: 'Delivery',
  DOSTAVKA_DATE: 'Will be comlete on',
  DROP_PLACE: 'Drop your phote here',
  TOTAL_PRICE: 'Order price',
  FOR_ALL: 'By default',
  CANCEL: 'Cancel',
  YOUR_PHOTO: 'Your photo',
  FILE_NAME: 'File name',
  PHOTO_QTY: 'Quantity',
  PHOTO_FORMAT: 'Format',
  PAPER_TYP: 'Paper',
  PHOTO_RAMKA: 'Frame',
  COMPLITE_ORDER: 'Order check out',
  SET_ALL_FORDEFAULT: 'Set current values as default'
};


 
app.controller('Ctrl', ['$translate', '$scope', function ($translate, $scope) {
 
  $scope.changeLanguage = function (langKey) {
    $translate.use(langKey);

    $translate('HEADLINE').then(function (headline) {
    $scope.headline = headline;
  }, function (translationId) {
    $scope.headline = translationId;
  });
  

  };

}]);