﻿<!DOCTYPE html>
<html ng-app="app">
<head>
    <meta charset="utf-8" />
    <title>AngularJS</title>
    <link rel="stylesheet" href="vendor/bootstrap.min.css" />
    <link href="app-content/app.css" rel="stylesheet" />
<!--     <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" crossorigin="anonymous"> -->
<base href="/">
</head>
<body>
   
        <div class="mar_auto">   
                <div ng-class="{ 'alert': flash, 'alert-success': flash.type === 'success', 'alert-danger': flash.type === 'error' }" ng-if="flash" ng-bind="flash.message"></div>
                <div ng-view></div>
        </div>
  
   

    <script src="vendor/jquery.js"></script>
    <script src="vendor/angular.min.js"></script>
    <script src="vendor/angular-route.min.js"></script>
    <script src="vendor/angular-cookies.min.js"></script>
    <script src="vendor/angular-animate.min.js"></script>
    <script src="vendor/angular-sanitize.min.js"></script>
    <script src="vendor/angular-translate.min.js"></script>
    <script src="vendor/ui-bootstrap.min.js"></script>
    <script src="vendor/ng-flow-standalone.js"></script>
    


    <script src="app.js"></script>

<script src="https://cdn.jsdelivr.net/satellizer/0.15.5/satellizer.min.js"></script>

    
   
    <script src="app-services/sumprice.service.js"></script>
    <script src="app-services/setdefault.service.js"></script>
   
    <!-- Real user service that uses an api -->
    <!-- <script src="app-services/user.service.js"></script> -->

    

    <script src="directive/dropdown.directive.js"></script>
    <script src="directive/directives.js"></script>
    <script src="directive/filter.js"></script>
    
    <script src="controllers/controller.js"></script>
    <script src="controllers/format.controller.js"></script>
    <script src="controllers/dostavka.controller.js"></script>
    <script src="controllers/name.controller.js"></script>
    <script src="controllers/counter.controller.js"></script>
    <script src="controllers/papertype.controller.js"></script>
    <script src="controllers/ramka.controller.js"></script>
    <script src="controllers/translate.controller.js"></script>

    <script src="upload/upload.controller.js"></script>
    

</body>
</html>