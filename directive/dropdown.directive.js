var app = angular.module('app');

app.directive('mydropdown', function(Sumprice) {
    return {
      restrict: 'E',
      require: '^ngModel',
      
      scope: {
        index: '@',
        ngModel: '=', // selection
        items: '=',   // items to select from
        callback: '&', // callback
        selectParam: '&',
        bydefault: '='
      },
     
      link: function(scope, element, attrs) {
        element.on('click', function(event) {
          event.preventDefault();
      });
         
        let tmp = scope.items[0];
        scope.default = tmp.name;

        scope.isButton = 'isButton' in attrs;
       
        // selection changed handler
        scope.select = function(item) {
          scope.ngModel = item;
          if (scope.callback) {
            scope.callback({ item: item });
          }

        };
      },
      templateUrl: 'directive/dropdown-template.html'
    };

  });