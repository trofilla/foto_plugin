﻿(function () {
    'use strict';

    angular
        .module('app', ['ngRoute', 'ngCookies', 'flow', 'ngAnimate', 'ui.bootstrap', 'ngSanitize', 'pascalprecht.translate'])
        .config(config)
        // .run(run);

    config.$inject = ['$routeProvider', '$locationProvider', '$translateProvider', 'flowFactoryProvider'];
    function config($routeProvider, $locationProvider, $translateProvider, flowFactoryProvider) {
      
        $locationProvider.hashPrefix('');

        flowFactoryProvider.defaults = {
            target: '../upload.php',
            permanentErrors: [404, 500, 501],
            maxChunkRetries: 1,
            chunkRetryInterval: 5000,
            simultaneousUploads: 1
            // query: myJsonName
            };
        flowFactoryProvider.on('catchAll', function (event, $file, fileSuccess) {
             // console.log('catchAll', arguments);
            });

        $translateProvider.translations('uk', translationsUK);
        $translateProvider.translations('ru', translationsRU);
        $translateProvider.translations('en', translationsEN);
        $translateProvider.preferredLanguage('uk');
        $translateProvider.fallbackLanguage('uk');

        $locationProvider.html5Mode(true);

        $routeProvider
            .when('/', {
                controller: 'UploadController',
                templateUrl: 'upload/upload.view.html'
            })

     
            .otherwise({ redirectTo: '' });
    }

  
})();
