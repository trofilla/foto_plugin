(function () {
    'use strict';

    angular
        .module('app')
        .factory('Sumprice', SumpriceService);

    SumpriceService.$inject = ['$rootScope', '$timeout', 'Setdefault'];
    function SumpriceService($rootScope, $timeout, Setdefault) {
     

    // let DefaultParam = {
    //   format: '10 x 15',
    //   qty: 1,
    //   ramka: 'Ні',
    //   paper: 'Глянцевий'
    // };

    let TotalFormatPrice = [];
    let TotalFormat = [];
    let TotalQty = [];
    let TotalRamka = [];
    let TotalPaper = [];
    let TotalName = [];
    let TotalPrice = 0;
    let TotalAdress = {};
    let defaultParams = Setdefault.getParam();
    let orderNumber;
  
  
    return {
    setorderNumber: function(number){
      orderNumber = number;
    },
    getorderNumber: function(){
     return orderNumber;
    },
    setAllDefault(){
      let defaultParams = Setdefault.getParam();
      console.log(defaultParams);
      return  defaultParams;
    },
    savePhone: function(item){
        TotalAdress.adress = item;
    },

    savePhone2: function(item){
        TotalAdress.phone = item;
    },

    savePrumitka: function(item){
        TotalAdress.prumitka = item;
    },
    totalAdress: function(){
      return TotalAdress;
    },


    allToJson: function(){
        // console.log(TotalFormat, TotalQty);
        let sum = new Array();
        let newsum = new Array();
        let param = new Object();
    

      for (var i = TotalName.length - 1; i >= 0; i--) {
        param.name = TotalName[i];
        param.format = TotalFormat[i]; 
        param.qty = TotalQty[i];
        param.paper = TotalPaper[i];
        param.ramka = TotalRamka[i]; 

        var obj = angular.toJson(param, true);

        
        sum[i]=obj;
      };

      for (var i = sum.length-1; i >= 0; i--){
        var her = angular.fromJson(sum[i]);
        newsum[i]=her;
      }
 
        return newsum;
      },

      totalPrice : function(){
       var TotalPrice = 0;
          for(var i=0; i< TotalQty.length; i++) {
              TotalPrice += TotalQty[i]*TotalFormatPrice[i];
          }

          $rootScope.TotalPrice = TotalPrice.toFixed(2);
          // console.log(TotalQty);
          // console.log(TotalFormatPrice);
        return TotalPrice;
      },
      formatPrice: function(format, index){

        TotalFormatPrice[index] = format;
        return TotalFormatPrice;
      },
      deleteFormatPrice(index){
        TotalFormatPrice.splice(index, 1);
        return TotalFormatPrice;
      },
      totalFormat: function(format, index){
        TotalFormat[index] = format;
        return TotalFormat;
      },
      totalPaper: function(format, index){
        TotalPaper[index] = format;
        return TotalPaper;
      },
      totalRamka: function(format, index){
        TotalRamka[index] = format;
        return TotalRamka;
      },
      totalName: function(format, index){
        TotalName[index] = format;
        return TotalName;
      },
      qtyPrice: function(format, index){
        TotalQty[index] = format;
        return TotalQty;
      }, 
      deleteQty: function(index){
        TotalQty.splice(index, 1);
        
        return TotalQty;
      }
    }

    }

})();
