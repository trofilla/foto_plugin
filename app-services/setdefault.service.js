(function () {
    'use strict';

    angular
        .module('app')
        .factory('Setdefault', SetdefaultService);

    SetdefaultService.$inject = ['$rootScope', '$timeout'];
    function SetdefaultService($rootScope, $timeout) {
     

    let DefaultParam = {
      format: '10 x 15',
      qty: 1,
      ramka: 'Ні',
      paper: 'Глянцевий'
    };

    return {
      setQty: function(item){
         DefaultParam.qty = item;
      },
      setParam: function(obj){
        DefaultParam.qty = obj.qty;
        DefaultParam.format = obj.format;
        DefaultParam.ramka = obj.ramka;
        DefaultParam.paper = obj.paper;
        
      },
      getParam: function(){

        return DefaultParam;
      }
    }

  }

})();
